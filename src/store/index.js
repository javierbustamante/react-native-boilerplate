import { applyMiddleware, createStore } from 'redux'
import rootReducer from '../reducers'
import { getServiceClientMiddleware } from '../utils/network/NetworkMiddleware'

const getNetworkMiddleware = getServiceClientMiddleware('')

const getMiddlewares = () => {
  if (__DEV__) {
    return applyMiddleware(getNetworkMiddleware)
  }
  return applyMiddleware(getNetworkMiddleware)
}

export const store = createStore(rootReducer, getMiddlewares())
